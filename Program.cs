﻿using System.IO;

namespace Tirage {
using System.Windows;
    public class Program{
        public static void Main(string[] args) {
            List<Maison> maisons = new List<Maison>();
            List<Personne> participants = new List<Personne>();
            List<Tirage> offres = new List<Tirage>();
            //Lecture des fichiers et creation des maisons
            maisons.AddRange(Lecteur.ReadFile("Noel2023.ini"));
            
            //Creation d'une liste de tout les participants
            foreach(Maison m in maisons) {
                participants.AddRange(m.habitants);
            }
            //Mise en place du tirage au sort
            foreach(Personne p in participants) {
                Tirage tmp = new Tirage(p, maisons);
                Personne tp = tmp.giver;
                foreach(Maison m in maisons) {
                    if(m.habitants.Contains(tp)) {
                        m.habitants.Remove(tp);
                        if(m.EstVide()) {
                            maisons.Remove(m);
                        }
                        break;
                    }
                }
                offres.Add(tmp);
            }
            //Affichage des resultats
            foreach(Tirage t in offres) {
                Console.WriteLine(t);
            }


            //MainForm window = new MainForm();
            //window.ShowDialog();
        }
    }
}
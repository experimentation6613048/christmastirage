namespace Tirage{
    public class Maison{
        public List<Personne> habitants{get; private set;}
        public string ville{get; private set;}

        public Maison(string ville) {
            this.habitants = new List<Personne>();
            this.ville = ville;
        }

        public void AddHabitant(Personne p) {
            this.habitants.Add(p);
        }

        public override string ToString() {
            string str = this.ville;
            str +="\n";
            foreach(Personne p in this.habitants) {
                str += p + "\t";
            }
            return str;
        }

        public bool Loge(Personne p) {
            return p.ville == this.ville;
        }

        public bool EstVide() {
            if(this.habitants.Count() == 0) {
                return true;
            }
            return false;
        }

    }
}
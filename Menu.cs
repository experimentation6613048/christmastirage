using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Tirage
{
    public class Menu : Panel
    {
        private Button tirage = new Button();
        private ParametresButton parametresButton = new ParametresButton();

        public event EventHandler QuitMenuClicked;
        public event EventHandler ParametresClicked;

        private bool isMouseDown = false;

        public Menu()
        {
            InitializeMenu();
            this.BackColor = Color.Transparent;
        }

        private void InitializeMenu()
        {
            this.Dock = DockStyle.Fill;

            // Créez un bouton pour quitter le menu
            tirage = new Button();
            tirage.Text = "Procéder au tirage";
            tirage.Size = new Size(this.Width / 5, this.Height / 15);

            // Définissez la couleur foncée pour le haut et la couleur claire pour le bas
            Color couleurFoncee = Color.DarkGoldenrod;
            Color couleurClaire = Color.Gold;

            tirage.Paint += (sender, e) =>
            {
                Rectangle rect = new Rectangle(0, 0, tirage.Width, tirage.Height);
                Color couleurActuelle = isMouseDown ? couleurFoncee : couleurClaire;
                using (LinearGradientBrush brush = new LinearGradientBrush(rect, couleurFoncee, couleurActuelle, LinearGradientMode.Vertical))
                {
                    e.Graphics.FillRectangle(brush, rect);
                }
            };

            tirage.FlatStyle = FlatStyle.Flat;
            tirage.FlatAppearance.BorderSize = 0;

            // Gérez l'événement MouseDown pour l'effet de clic
            tirage.MouseDown += (sender, e) =>
            {
                isMouseDown = true;
                tirage.Invalidate(); // Redessiner le bouton pour afficher l'effet de clic
            };

            // Gérez l'événement MouseUp pour réinitialiser l'état du bouton
            tirage.MouseUp += (sender, e) =>
            {
                isMouseDown = false;
                tirage.Invalidate(); // Redessiner le bouton pour rétablir l'apparence par défaut
            };

            tirage.Click += (sender, e) =>
            {
                OnQuitMenuClicked();
            };

            this.Controls.Add(tirage);


            // Créez un bouton de paramètres
            parametresButton = new ParametresButton();
            this.Controls.Add(parametresButton);

            // Gestion de l'événement du bouton "Paramètres" pour ouvrir les paramètres
            parametresButton.Click += (sender, e) =>
            {
                OnParametresClicked();
            };

            // Abonnez-vous à l'événement SizeChanged
            this.SizeChanged += MainMenu_SizeChanged;
        }


        // Méthode pour déclencher l'événement ParametresClicked
        protected virtual void OnParametresClicked()
        {
            ParametresClicked?.Invoke(this, EventArgs.Empty);
        }
        protected virtual void OnQuitMenuClicked()
        {
            QuitMenuClicked?.Invoke(this, EventArgs.Empty);
        }

        private void MainMenu_SizeChanged(object? sender, EventArgs e)
        {
            // Redimensionnez les boutons lorsque la taille du menu change
            tirage.Size = new Size(this.Width / 5, this.Height / 15);
            tirage.Location = new Point((this.Width - tirage.Width) / 2, (this.Height - tirage.Height) / 2);

            parametresButton.Size = new Size(tirage.Height, tirage.Height);
            parametresButton.Location = new Point(this.Width - parametresButton.Width - 10, 10);
        }

        
    }
}

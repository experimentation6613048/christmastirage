
namespace Tirage{
    public static class Lecteur{

        public static List<Maison> ReadFile(string cheminFichier) {
            List<Maison> maisons = new List<Maison>();
            Maison maison = new Maison("témoin");
            
            foreach (string ligne in File.ReadLines(cheminFichier)) {
                if (ligne.StartsWith("[")) {
                    if(maison.ville != "témoin") {
                        maisons.Add(maison);
                    }
                    maison = new Maison(ligne.Trim('[', ']'));
                }
                else {
                    if(maison.ville != "témoin") {
                        if(!string.IsNullOrWhiteSpace(ligne)) maison.AddHabitant(new Personne(ligne, maison.ville));
                    }
                    
                }
            }
            maisons.Add(maison);
            
            return maisons;
        }
    }
}


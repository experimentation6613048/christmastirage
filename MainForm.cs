using System.Windows.Forms;
using System;
using System.Drawing;
namespace Tirage {
    public class MainForm : Form {

        private Menu mainMenu;
        public MainForm() {

            InitializeForm();

            this.BackgroundImage = Image.FromFile("Photo/FondMain.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            mainMenu = new Menu();
            mainMenu.QuitMenuClicked += (sender, e) =>
            {
                QuitMenu(); // Appel de la fonction QuitMenu du MainForm
            };
            this.Controls.Add(mainMenu);

            InitializeForm();
        }

        private void InitializeForm()
        {
            // Configure le formulaire principal
            this.Text = "ChristmasTirage";
            this.StartPosition = FormStartPosition.CenterScreen;
            if(Screen.PrimaryScreen == null) {
                ErrorMessage.Show("Error Screen Properties");
                return;
            }
            this.Size = new Size(Screen.PrimaryScreen.Bounds.Width / 2, Screen.PrimaryScreen.Bounds.Height / 2);
        }


        private void QuitMenu()
        {
            this.Controls.Remove(mainMenu);
            Defile();
        }

        private void Defile()
        {
            List<string> args = new List<string>();
            List<Maison> maisons = new List<Maison>();
            List<Personne> participants = new List<Personne>();
            List<Tirage> offres = new List<Tirage>();
            //Lecture des fichiers et creation des maisons
            foreach(string file in args) {
                if(File.Exists(file)) {
                    maisons.AddRange(Lecteur.ReadFile(file));
                }
                else{
                    ErrorMessage.Show("Fichier "+ file + " inexistant");
                }
            }
            //Creation d'une liste de tout les participants
            foreach(Maison m in maisons) {
                participants.AddRange(m.habitants);
            }
            //Mise en place du tirage au sort
            foreach(Personne p in participants) {
                Tirage tmp = new Tirage(p, maisons);
                Personne tp = tmp.giver;
                foreach(Maison m in maisons) {
                    if(m.habitants.Contains(tp)) {
                        m.habitants.Remove(tp);
                        if(m.EstVide()) {
                            maisons.Remove(m);
                        }
                        break;
                    }
                }
                offres.Add(tmp); 
            }
            //Affichage des resultats
        }

        
    }
}

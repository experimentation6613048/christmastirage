using System.Collections.Generic;

namespace Tirage{
    public class Tirage{
        public List<Personne> gamble{get; private set;}
        public Personne gifted{get; private set;}
        public Personne giver{get; private set;}

        public Tirage(Personne gifted, List<Maison> maisons) {
            this.gamble = new List<Personne>();
            this.gifted = gifted;
            
            foreach(Maison m in maisons) {
                if(!m.Loge(gifted) && m.habitants.Count() > 1) {
                    this.gamble.AddRange(m.habitants);
                }
            }
            if(this.gamble.Count() == 0) {
                foreach(Maison m in maisons) {
                    if(!m.Loge(gifted)) {
                        this.gamble.AddRange(m.habitants);
                    }
                }
            }
            if(this.gamble.Count() == 0) {
                foreach(Maison m in maisons) {
                    this.gamble.AddRange(m.habitants);
                }
            }
            if(this.gamble.Count() == 0) {
                ErrorMessage.Show("Plus de donneur pour "+ this.gifted);
            }
            Shuffle();
            int r = new Random().Next()%this.gamble.Count();
            this.giver = this.gamble[r];
        }

        private void Shuffle() {
            Random rand = new Random();
            int n = gamble.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                Personne value = gamble[k];
                gamble[k] = gamble[n];
                gamble[n] = value;
            }
        }

        public override string ToString() {
            return this.giver + " offre a " + this.gifted;
        }
    }
}
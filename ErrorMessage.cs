using System.Windows.Forms;

namespace Tirage {


    public static class ErrorMessage{
        public static void Print(string error) {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(error);
            Console.ResetColor();
        }
        public static void Show(string error){
            MessageBox.Show("Erreur",error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
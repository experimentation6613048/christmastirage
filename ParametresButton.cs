using System;
using System.Drawing;
using System.Windows.Forms;

namespace Tirage {
    public class ParametresButton : Button {

        public ParametresButton()
        {
            // Supprimez le texte
            this.Text = "";

            // Supprimez le style de bouton
            this.FlatStyle = FlatStyle.Flat;

            // Supprimez la bordure
            this.FlatAppearance.BorderSize = 0;

            // Définissez le fond comme transparent
            this.BackColor = Color.Transparent;

            // Chargez l'icône de l'engrenage
            this.BackgroundImage = Image.FromFile("Photo/engrenage.png");

            // Ajustez la taille du bouton en fonction de la taille de l'icône
            this.Size = new Size(20,20);

            // Gestion de l'événement pour personnaliser l'effet au survol
            this.MouseEnter += (sender, e) =>
            {
                this.FlatAppearance.BorderColor = Color.Black; // Bordure noire au survol
                this.FlatAppearance.BorderSize = 2; // Épaisseur de la bordure au survol
            };

            this.MouseLeave += (sender, e) =>
            {
                this.FlatAppearance.BorderSize = 0; // Supprime la bordure au départ du survol
            };
        }
    }
}
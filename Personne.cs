namespace Tirage{
    public class Personne {
        public string name{get; private set;}
        public string ville{get; private set;}

        public Personne(string name, string ville) {
            this.name = name;
            this.ville = ville;
        }

        public override string ToString() {
            return this.name;
        }
    }
}